# When updating version make sure to check on semgrepignore file as well
ARG PROJECT_VERSION=0.0.1
FROM golang:1.17-alpine AS build
ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /app

COPY go.* ./

RUN go mod download

COPY . ./

# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=` go list -m` && \
        go build -v -ldflags="-X '$PATH_TO_MODULE/metadata.ProjectVersion=$CHANGELOG_VERSION'" -o scaffolder

FROM debian:buster-slim

RUN set -x && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
     ca-certificates && \
     rm -rf /var/lib/apt/lists/*

COPY --from=build /app/scaffolder /scaffolder
COPY --from=build /app/template /template

CMD ["/scaffolder", "run"]
