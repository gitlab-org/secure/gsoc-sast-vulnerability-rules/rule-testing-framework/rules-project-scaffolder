package main

import (
	"log"
	"os"
	"rules-project-scaffolder/cmd"
	"rules-project-scaffolder/scaffold"
)

func main() {
	app := cmd.New()

	// register scaffold on CLI
	app.Commands = append(app.Commands, scaffold.CliCommand())

	if err := cmd.Execute(app, os.Args); err != nil {
		log.Fatal(err)
	}
}
