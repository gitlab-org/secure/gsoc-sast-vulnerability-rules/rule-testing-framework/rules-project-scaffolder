package build

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"rules-project-scaffolder/build/inlinedep"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

const (
	depSeqString = "libraryDependencies ++= Seq(\n%s\n    )"
	depTemplate  = "      \"%s\" %% \"%s\" %% \"%s\" %% \"compile\"%s"
)

type sbtBuilder struct{}

// parseDeps implements Builder
func (s sbtBuilder) parseDeps(opts parseDepsOpts) ([]Dependency, error) {
	var sbtDeps []Dependency

	// inline dependencies
	inlineDeps, err := s.parseInlineDeps(opts.InlineFiles)
	if err != nil {
		return nil, errors.Wrap(err, "parse inline")
	}
	sbtDeps = append(sbtDeps, inlineDeps...)

	// raw dependencies
	rawDeps, err := s.parseRawDeps(opts.RawDeps)
	if err != nil {
		return nil, err
	}
	sbtDeps = append(sbtDeps, rawDeps...)

	return sbtDeps, nil
}

func (s sbtBuilder) parseRawDeps(raw []RawDependency) ([]Dependency, error) {
	var sbtDeps []Dependency
	in, err := yaml.Marshal(raw)
	if err != nil {
		return nil, errors.Wrap(err, "parse raw deps")
	}
	var rawSbtDeps []mvnDependency
	if err := yaml.Unmarshal(in, &rawSbtDeps); err != nil {
		return nil, errors.Wrap(err, "map raw to sbt deps")
	}
	for _, sbtDep := range rawSbtDeps {
		if !sbtDep.Valid() {
			return nil, fmt.Errorf("invalid dependency - groupId: %s, artificatId: %s", sbtDep.GroupID, sbtDep.ArtifactID)
		}
		sbtDeps = append(sbtDeps, sbtDep)
	}
	return sbtDeps, nil
}

func (s sbtBuilder) parseInlineDeps(filePaths []string) ([]Dependency, error) {
	var sbtDeps []Dependency
	inlineDeps, err := inlinedep.Scan(filePaths)
	if err != nil {
		return nil, errors.Wrap(err, "parse inline")
	}
	for _, idep := range inlineDeps {
		name := idep.Name()
		idx := strings.LastIndex(name, ".")
		groupID, artifactID := name[:idx], name[idx+1:]
		sbtDeps = append(sbtDeps, mvnDependency{
			GroupID:    groupID,
			ArtifactID: artifactID,
			Version:    idep.Ver(),
		})
	}
	return sbtDeps, nil
}

func (s sbtBuilder) Name() Tool {
	return SBT
}

func (s sbtBuilder) Template() string {
	return "build.sbt"
}

func (s sbtBuilder) Extensions() []string {
	return []string{".scala", ".sc"}
}

func (s sbtBuilder) Supports(ext string) bool {
	for _, fileExt := range s.Extensions() {
		if ext == fileExt {
			return true
		}
	}
	return false
}

func (s sbtBuilder) GenerateManifest(deps []Dependency) ([]byte, error) {
	templBytes, _, err := readBuildTemplate(s.Template())
	if err != nil {
		return nil, errors.Wrap(err, "reading SBT template")
	}

	sbtTempl := template.New(s.Template())
	sbtFile, err := sbtTempl.Parse(string(templBytes))
	if err != nil {
		return nil, errors.Wrap(err, "reading SBT template")
	}

	depString := ""

	for i, dep := range deps {
		if !dep.Valid() {
			return nil, errors.New("invalid dependency")
		}
		sbtDep, ok := dep.(mvnDependency)

		if !ok {
			return nil, errors.New("invalid SBT dependency format")
		}

		lineTermination := ",\n"
		if i == len(deps)-1 {
			lineTermination = ""
		}

		depString += fmt.Sprintf(depTemplate, sbtDep.GroupID, sbtDep.ArtifactID, sbtDep.Ver(), lineTermination)
	}
	if len(deps) > 0 {
		depString = fmt.Sprintf(depSeqString, depString)
	}
	var manifestBuffer bytes.Buffer

	_ = sbtFile.Execute(&manifestBuffer, map[string]string{
		"depString": depString,
	})

	return manifestBuffer.Bytes(), nil
}

func (s sbtBuilder) GenerateProject(opts ProjStructOps) (string, error) {
	if err := opts.normalize(); err != nil {
		return "", err
	}

	srcDirPath := opts.SrcPath
	targetDirPath := filepath.Join(opts.OutPath, ProjectName)

	if _, e := os.Stat(targetDirPath); e == nil {
		return "", fmt.Errorf("folder already exists at target path: '%s', delete the folder or provide different target path", targetDirPath)
	}

	err := func() error {
		// create the output folder
		// language files inside 'src/main/scala'
		targetInnerDirPath := filepath.Join(targetDirPath, "src", "main", "scala")
		fmt.Println(srcDirPath, targetInnerDirPath, targetDirPath)
		// create folders
		err := os.MkdirAll(targetInnerDirPath, os.ModePerm)
		if err != nil && !os.IsExist(err) {
			return errors.Wrap(err, "create project dir")
		}

		// add language files
		for _, filePath := range opts.LangFiles {
			retainDirStruct := true // retain dir structure for lang files
			if err := writeFile(writeFileOpts{srcDirPath, filePath, targetInnerDirPath, opts.TrimFileNamePrefixes, retainDirStruct}); err != nil {
				return fmt.Errorf("add src file(%s): %w", filepath.Base(filePath), err)
			}
		}
		// add auxiliary files
		for _, filePath := range opts.AuxFiles {
			retainDirStruct := false // do not retain directory structure for aux files, place all files at root
			if err := writeFile(writeFileOpts{srcDirPath, filePath, targetDirPath, opts.TrimFileNamePrefixes, retainDirStruct}); err != nil {
				return fmt.Errorf("add aux file(%s): %w", filepath.Base(filePath), err)
			}
		}

		// generate manifest
		sbtBytes, err := s.GenerateManifest(opts.Deps)
		if err != nil {
			return err
		}
		sbtFilePath := filepath.Join(targetDirPath, s.Template())
		err = os.WriteFile(sbtFilePath, sbtBytes, 0o644)
		if err != nil {
			return errors.Wrap(err, "add build file")
		}
		return nil
	}()
	if err != nil {
		_ = os.RemoveAll(targetDirPath) // remove project folder on error
		return "", err
	}

	return targetDirPath, nil
}
