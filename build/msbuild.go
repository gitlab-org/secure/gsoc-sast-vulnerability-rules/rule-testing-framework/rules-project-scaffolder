package build

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

type msbuildBuilder struct{}

func (m msbuildBuilder) Name() Tool {
	return MSBuild
}

func (m msbuildBuilder) Template() string {
	return "test.csproj"
}

func (m msbuildBuilder) Extensions() []string {
	return []string{".cs"}
}

func (m msbuildBuilder) Supports(ext string) bool {
	for _, fileExt := range m.Extensions() {
		if ext == fileExt {
			return true
		}
	}
	return false
}

func (m msbuildBuilder) parseDeps(opts parseDepsOpts) (deps []Dependency, err error) {
	return deps, nil
}

func (m msbuildBuilder) parseInlineDeps(filePaths []string) (deps []Dependency, err error) {
	return deps, nil
}

func (m msbuildBuilder) parseRawDeps(raw []RawDependency) (deps []Dependency, err error) {
	return deps, nil
}

func (m msbuildBuilder) GenerateManifest(deps []Dependency) ([]byte, error) {
	csproj, _, err := readBuildTemplate(m.Template())
	if err != nil {
		return nil, err
	}

	return csproj, nil
}

func (m msbuildBuilder) GenerateProject(opts ProjStructOps) (string, error) {
	if err := opts.normalize(); err != nil {
		return "", err
	}
	srcDirPath := opts.SrcPath
	targetDirPath := filepath.Join(opts.OutPath, ProjectName)

	if _, e := os.Stat(targetDirPath); e == nil {
		return "", fmt.Errorf("folder already exists at target path: '%s', delete the folder or provide different target path", targetDirPath)
	}

	err := func() error {
		// create the output folder
		err := os.MkdirAll(targetDirPath, os.ModePerm)
		if err != nil && !os.IsExist(err) {
			return errors.Wrap(err, "create project dir")
		}

		// add language files
		for _, filePath := range opts.LangFiles {
			retainDirStruct := true // retain dir structure for lang files
			if err := writeFile(writeFileOpts{srcDirPath, filePath, targetDirPath, opts.TrimFileNamePrefixes, retainDirStruct}); err != nil {
				return fmt.Errorf("add src file(%s): %w", filepath.Base(filePath), err)
			}
		}
		// add auxiliary files
		for _, filePath := range opts.AuxFiles {
			retainDirStruct := false // do not retain directory structure for aux files, place all files at root
			if err := writeFile(writeFileOpts{srcDirPath, filePath, targetDirPath, opts.TrimFileNamePrefixes, retainDirStruct}); err != nil {
				return fmt.Errorf("add aux file(%s): %w", filepath.Base(filePath), err)
			}
		}

		// generate manifest
		pomBytes, err := m.GenerateManifest(opts.Deps)
		if err != nil {
			return err
		}
		pomFilePath := filepath.Join(targetDirPath, m.Template())
		err = os.WriteFile(pomFilePath, pomBytes, 0o644)
		if err != nil {
			return errors.Wrap(err, "add build file")
		}
		return nil
	}()
	if err != nil {
		_ = os.RemoveAll(targetDirPath) // remove project folder on error
		return "", err
	}

	return targetDirPath, nil
}
