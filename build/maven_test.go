package build

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/creekorful/mvnparser"
	"github.com/stretchr/testify/assert"
)

func TestMavenBuilder_Meta(t *testing.T) {
	mvn := mvnBuilder{}
	assert.Equal(t, Maven, mvn.Name())
	assert.Equal(t, "pom.xml", mvn.Template())
	supported := []string{".java", ".kt", ".groovy"}
	unsupported := []string{randomExt(), randomExt(), randomExt()}
	for _, lang := range supported {
		lang := lang
		t.Run("support for "+lang, func(t *testing.T) {
			if !mvn.Supports(lang) {
				t.Errorf("unsupported language")
				return
			}
		})
	}
	for _, lang := range unsupported {
		lang := lang
		t.Run("no support for "+lang, func(t *testing.T) {
			if mvn.Supports(lang) {
				t.Errorf("should not have supported but is supporting")
				return
			}
		})
	}
}

func TestMavenBuilder_GenerateManifest(t *testing.T) {
	mvn := mvnBuilder{}
	currentDir, _ := os.Getwd()

	t.Run("no dependencies", func(t *testing.T) {
		// generated POM should be same as template file
		got, err := mvn.GenerateManifest(nil)
		if err != nil {
			t.Error(err)
			return
		}
		tmplPath := filepath.Join(currentDir, "..", "template", "pom.xml")
		want, err := os.ReadFile(tmplPath)
		if err != nil {
			t.Errorf("read template file: %s", err)
			return
		}
		assert.True(t, bytes.Equal(got, want))
	})

	t.Run("invalid dependency format", func(t *testing.T) {
		deps := []Dependency{
			mockDependency{
				name:    "",
				version: "1.2.3",
			},
		}
		if _, err := mvn.GenerateManifest(deps); err == nil {
			t.Errorf("expected invalid maven dependency format but got empty")
		}
	})

	t.Run("invalid dependency data", func(t *testing.T) {
		deps := []Dependency{
			mvnDependency{
				GroupID: "",
				Version: "1.2.3",
			}, mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "",
			},
		}
		if _, err := mvn.GenerateManifest(deps); err == nil {
			t.Errorf("expected invalid dependency information but got empty")
		}
	})

	t.Run("valid dependencies: via data structure", func(t *testing.T) {
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			}, mvnDependency{
				GroupID:    "org.gitlab",
				ArtifactID: "test",
				Version:    "RELEASE",
			},
		}

		// generated POM should be same as template file
		got, err := mvn.GenerateManifest(deps)
		if err != nil {
			t.Errorf("expected no error but got: %s", err)
			return
		}

		// Load project from string
		var project mvnparser.MavenProject
		if err := xml.Unmarshal(got, &project); err != nil {
			t.Fatalf("unable to unmarshal pom file, error: %s", err)
			return
		}

		assert.Equal(t, len(deps), len(project.Dependencies))

		for _, mvnDep := range project.Dependencies {
			dep := mockDependency{
				name:    mvnDep.GroupId + "." + mvnDep.ArtifactId,
				version: mvnDep.Version,
			}
			assert.Truef(t, exists(deps, dep), "dep[%s] missing in POM", dep)
		}
	})

	t.Run("valid dependencies: via scaffold config ", func(t *testing.T) {
		currentDir, _ := os.Getwd()
		mvn := mvnBuilder{}
		projSrc := filepath.Join(currentDir, "..", "testdata", "mavenproject")

		inlineFiles := []string{
			filepath.Join(projSrc, "App.java"),
			filepath.Join(projSrc, "dir1", "TestDir1.java"), // 1 dep
			filepath.Join(projSrc, "dir2", "Dir2.java"),     // 2 dep(1 duplicate)
		}

		rawDeps, err := readDepsFromCfg(filepath.Join(projSrc, "scaffold.yml"))
		if err != nil {
			t.Error(err)
			return
		}

		mvnDeps, err := mvn.parseDeps(parseDepsOpts{
			InlineFiles: inlineFiles,
			RawDeps:     rawDeps,
		})
		if err != nil {
			t.Errorf("unexpected error while parsing deps - %s", err)
			return
		}

		// generated POM should be same as template file
		got, err := mvn.GenerateManifest(mvnDeps)
		if err != nil {
			t.Errorf("expected no error but got: %s", err)
			return
		}

		// Load project from string
		var project mvnparser.MavenProject
		if err := xml.Unmarshal(got, &project); err != nil {
			t.Fatalf("unable to unmarshal pom file, error: %s", err)
			return
		}

		assert.Equal(t, len(mvnDeps), len(project.Dependencies))

		for _, mvnDep := range project.Dependencies {
			dep := mockDependency{
				name:    mvnDep.GroupId + "." + mvnDep.ArtifactId,
				version: mvnDep.Version,
			}
			assert.Truef(t, exists(mvnDeps, dep), "dep[%s] missing in POM", dep)
		}
	})
}

func TestMavenBuilder_GenerateProject(t *testing.T) {
	currentDir, _ := os.Getwd()
	mvn := mvnBuilder{}

	t.Run("invalid options", func(t *testing.T) {
		variants := []struct {
			caseName string
			opts     ProjStructOps
		}{
			{"empty", ProjStructOps{}},
			{"no source path", ProjStructOps{
				SrcPath:   "",
				Deps:      nil,
				LangFiles: []string{"test"},
			}},
			{"no language files", ProjStructOps{
				SrcPath:   currentDir,
				LangFiles: nil,
			}},
		}
		for i := range variants {
			variant := variants[i]
			t.Run(variant.caseName, func(t *testing.T) {
				_, err := mvn.GenerateProject(variant.opts)
				if err == nil {
					t.Errorf("expected error but got empty")
				}
			})
		}
	})

	t.Run("valid case: no dependencies", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "mavenproject")
		langFiles, _ := getFiles(t, srcPath, mvn)
		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			LangFiles: langFiles,
		}
		outPath, err := mvn.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles, mvn, "src", "main")
	})

	t.Run("valid case: with dependencies", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "mavenproject")
		langFiles, _ := getFiles(t, srcPath, mvn)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			},
		}

		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			Deps:      deps,
			LangFiles: langFiles,
		}
		outPath, err := mvn.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles, mvn, "src", "main")
	})

	t.Run("valid case: with aux files", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "mavenproject")
		langFiles, auxFiles := getFiles(t, srcPath, mvn)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			},
		}

		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			Deps:      deps,
			LangFiles: langFiles,
			AuxFiles:  auxFiles,
		}
		outPath, err := mvn.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles, mvn, "src", "main")
	})

	t.Run("valid case: with trim file name", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "mavenproject")
		langFiles, auxFiles := getFiles(t, srcPath, mvn)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			},
		}

		fmt.Printf("%#v\n", langFiles)

		trimFileNames := []string{"Test", "Foo"}

		opts := ProjStructOps{
			SrcPath:              srcPath,
			OutPath:              currentDir,
			Deps:                 deps,
			LangFiles:            langFiles,
			AuxFiles:             auxFiles,
			TrimFileNamePrefixes: trimFileNames,
		}
		outPath, err := mvn.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)

		assert.DirExists(t, outPath, "generated project directory does not exist")
		outLangFiles, outAuxFiles := getFiles(t, outPath, mvn)
		// confirm if file paths are preserved in the generated project
		outSrcPath := filepath.Join(outPath, "src", "main") // project files are placed in <project>/src/main/<...files..>
		for _, outFile := range outLangFiles {
			fpath := strings.TrimPrefix(outFile, outSrcPath)
			if hasFilePath(langFiles, fpath) {
				continue
			}

			t.Logf("%s is not found in language files; checking for untrimmed version", fpath)

			foundUntrimmed := false
			for _, prfx := range trimFileNames {
				untrimmedFileName := prfx + filepath.Base(fpath)
				untrimmedFilePath := filepath.Join(filepath.Dir(fpath), untrimmedFileName)
				if hasFilePath(langFiles, untrimmedFilePath) {
					t.Logf("found untrimmed version %s", untrimmedFilePath)
					foundUntrimmed = true
					break
				}
			}
			if !assert.Truef(t, foundUntrimmed, "file path structure for file[%s] not retained", fpath) {
				return
			}
		}

		var containsBuildFile bool // to confirm if build file is present

		for _, outFile := range outAuxFiles {
			if strings.HasSuffix(outFile, mvnBuilder{}.Template()) {
				containsBuildFile = true
				continue
			}
			filePath := strings.TrimPrefix(outFile, outPath) // aux files are placed in the root level
			if !hasFilePath(auxFiles, filePath) {
				t.Errorf("file path structure for file[%s] not retained", filePath)
				return
			}
		}
		assert.True(t, containsBuildFile, "build file is missing")
	})
}

func TestMavenBuilder_ParseInlineDeps(t *testing.T) {
	currentDir, _ := os.Getwd()
	mvn := mvnBuilder{}
	projSrc := filepath.Join(currentDir, "..", "testdata", "mavenproject")
	files := []string{
		filepath.Join(projSrc, "App.java"),
		filepath.Join(projSrc, "dir1", "TestDir1.java"), // 1 dep
		filepath.Join(projSrc, "dir2", "Dir2.java"),     // 2 dep(1 duplicate)
	}
	expectedDeps := 2
	deps, err := mvn.parseInlineDeps(files)
	if err != nil {
		t.Errorf("expected no error but got: %s", err)
		return
	}
	assert.Len(t, deps, expectedDeps, "total expected deps mismatch")
	for _, d := range deps {
		assert.Truef(t, d.Valid(), "dependency invalid: %f", d.UID())
	}
}

func TestMavenBuilder_ParseRawDeps(t *testing.T) {
	mvn := mvnBuilder{}
	tests := []struct {
		usecase string
		dep     RawDependency
		valid   bool
	}{
		{
			usecase: "random info",
			dep: RawDependency{
				"random": "test",
			},
			valid: false,
		},
		{
			usecase: "missing group id",
			dep: RawDependency{
				"groupId": "org.gitlab",
			},
			valid: false,
		},
		{
			usecase: "missing artifact id",
			dep: RawDependency{
				"groupId": "org.gitlab",
			},
			valid: false,
		},
		{
			usecase: "valid case with version",
			dep: RawDependency{
				"groupId":    "org.gitlab",
				"artifactId": "test",
				"version":    "1.2.3",
			},
			valid: true,
		},
		{
			usecase: "valid case with meta attributes",
			dep: RawDependency{
				"groupId":    "org.gitlab",
				"artifactId": "test",
				"version":    "1.2.3",
				"exclusions": []map[string]interface{}{{
					"exclusion": map[string]interface{}{
						"groupId":    "org.gitlab",
						"artifactId": "test",
					},
				}},
			},
			valid: true,
		},
	}

	for _, variant := range tests {
		variant := variant
		t.Run(variant.usecase, func(t *testing.T) {
			deps, err := mvn.parseRawDeps([]RawDependency{variant.dep})
			if variant.valid {
				if err != nil {
					t.Errorf("expected no error but got - %s", err)
					return
				}
				assert.Len(t, deps, 1)
			} else {
				if err == nil {
					t.Errorf("expected invalid dependency error but got empty")
					return
				}
				assert.Len(t, deps, 0)
			}
		})
	}
}
