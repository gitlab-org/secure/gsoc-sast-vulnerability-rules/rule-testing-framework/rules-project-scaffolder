package build

import (
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSbtBuilder_Meta(t *testing.T) {
	sbt := sbtBuilder{}

	assert.Equal(t, SBT, sbt.Name())
	assert.Equal(t, "build.sbt", sbt.Template())

	supported := []string{".scala", ".sc"}
	unsupported := []string{randomExt(), randomExt(), randomExt()}

	for _, lang := range supported {
		t.Run("support for "+lang, func(t *testing.T) {
			if !sbt.Supports(lang) {
				t.Errorf("unsupported language")
				return
			}
		})
	}
	for _, lang := range unsupported {
		lang := lang
		t.Run("no support for "+lang, func(t *testing.T) {
			if sbt.Supports(lang) {
				t.Errorf("should not have supported but is supporting")
				return
			}
		})
	}
}

func TestSbtBuilder_GenerateManifest(t *testing.T) {
	sbtBuilder := sbtBuilder{}

	t.Run("no dependencies", func(t *testing.T) {
		got, err := sbtBuilder.GenerateManifest(nil)
		if err != nil {
			t.Error(err)
			return
		}

		want := "ThisBuild / scalaVersion     := \"2.13.8\"\nThisBuild / version          := \"0.1.0-SNAPSHOT\"\nThisBuild / organization     := \"com.gitlab\"\nThisBuild / organizationName := \"secure\"\n\nlazy val root = (project in file(\".\"))\n  .settings(\n    name := \"test-scala\",\n    \n  )\n"

		assert.Equal(t, want, string(got))
	})

	t.Run("invalid dependency format", func(t *testing.T) {
		deps := []Dependency{
			mockDependency{
				name:    "",
				version: "1.2.3",
			},
		}
		if _, err := sbtBuilder.GenerateManifest(deps); err == nil {
			t.Errorf("expected invalid dependency format but got empty")
		}
	})

	t.Run("invalid dependency data", func(t *testing.T) {
		deps := []Dependency{
			mvnDependency{
				GroupID: "",
				Version: "1.2.3",
			}, mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "",
			},
		}
		if _, err := sbtBuilder.GenerateManifest(deps); err == nil {
			t.Errorf("expected invalid dependency information but got empty")
		}
	})

	t.Run("valid dependencies", func(t *testing.T) {
		got, err := sbtBuilder.GenerateManifest([]Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			},
			mvnDependency{
				GroupID:    "org.gitlab",
				ArtifactID: "test",
				Version:    "RELEASE",
			},
		})
		if err != nil {
			t.Error(err)
			return
		}

		want := `ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.gitlab"
ThisBuild / organizationName := "secure"

lazy val root = (project in file("."))
  .settings(
    name := "test-scala",
    libraryDependencies ++= Seq(
      "com.gitlab" % "test" % "1.2.3" % "compile",
      "org.gitlab" % "test" % "RELEASE" % "compile"
    )
  )
`

		assert.Equal(t, want, string(got))
	})
}

func TestSbtBuilder_GenerateProject(t *testing.T) {
	currentDir, _ := os.Getwd()
	sbtBuilder := sbtBuilder{}

	t.Run("invalid options", func(t *testing.T) {
		variants := []struct {
			caseName string
			opts     ProjStructOps
		}{
			{"empty", ProjStructOps{}},
			{"no source path", ProjStructOps{
				SrcPath:   "",
				Deps:      nil,
				LangFiles: []string{"test"},
			}},
			{"no language files", ProjStructOps{
				SrcPath:   currentDir,
				LangFiles: nil,
			}},
		}
		for i := range variants {
			variant := variants[i]
			t.Run(variant.caseName, func(t *testing.T) {
				_, err := sbtBuilder.GenerateProject(variant.opts)
				if err == nil {
					t.Errorf("expected error but got empty")
				}
			})
		}
	})

	t.Run("valid case: no dependencies", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "scalaproject")
		langFiles, _ := getFiles(t, srcPath, sbtBuilder)
		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			LangFiles: langFiles,
		}
		outPath, err := sbtBuilder.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles, sbtBuilder, "src", "main", "scala")
	})

	t.Run("valid case: with dependencies", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "scalaproject")
		langFiles, _ := getFiles(t, srcPath, sbtBuilder)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			},
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			},
		}

		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			Deps:      deps,
			LangFiles: langFiles,
		}
		outPath, err := sbtBuilder.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles, sbtBuilder, "src", "main", "scala")
	})

	t.Run("valid case: with aux files", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "scalaproject")
		langFiles, auxFiles := getFiles(t, srcPath, sbtBuilder)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			},
		}

		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			Deps:      deps,
			LangFiles: langFiles,
			AuxFiles:  auxFiles,
		}
		outPath, err := sbtBuilder.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles, sbtBuilder, "src", "main", "scala")
	})

	t.Run("valid case: with trim file name", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "scalaproject")
		langFiles, auxFiles := getFiles(t, srcPath, sbtBuilder)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			},
		}

		trimFileName := "Test"

		opts := ProjStructOps{
			SrcPath:              srcPath,
			OutPath:              currentDir,
			Deps:                 deps,
			LangFiles:            langFiles,
			AuxFiles:             auxFiles,
			TrimFileNamePrefixes: []string{trimFileName},
		}
		outPath, err := sbtBuilder.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)

		assert.DirExists(t, outPath, "generated project directory does not exist")
		outLangFiles, outAuxFiles := getFiles(t, outPath, sbtBuilder)
		// confirm if file paths are preserved in the generated project
		outSrcPath := filepath.Join(outPath, "src", "main", "scala") // project files are placed in <project>/src/main/<...files..>
		for _, outFile := range outLangFiles {
			fpath := strings.TrimPrefix(outFile, outSrcPath)
			isNotInPath := !hasFilePath(langFiles, fpath)
			if isNotInPath {
				// simulate filepath before trimming and check if the path exists in
				// source lang file
				prevFileName := trimFileName + filepath.Base(fpath)
				prevFilePath := filepath.Join(filepath.Dir(fpath), prevFileName)
				if !hasFilePath(langFiles, prevFilePath) {
					t.Errorf("file path structure for file[%s] not retained", fpath)
					return
				}
			}
		}

		var containsBuildFile bool // to confirm if build file is present

		for _, outFile := range outAuxFiles {
			if strings.HasSuffix(outFile, sbtBuilder.Template()) {
				containsBuildFile = true
				continue
			}
			filePath := strings.TrimPrefix(outFile, outPath) // aux files are placed in the root level
			if !hasFilePath(auxFiles, filePath) {
				t.Errorf("file path structure for file[%s] not retained", filePath)
				return
			}
		}
		assert.True(t, containsBuildFile, "build file is missing")
	})
}

func TestSbtBuilder_ParseInlineDeps(t *testing.T) {
	currentDir, _ := os.Getwd()
	sbt := sbtBuilder{}
	projSrc := filepath.Join(currentDir, "..", "testdata", "scalaproject")
	files := []string{
		filepath.Join(projSrc, "Main.scala"),
		filepath.Join(projSrc, "dir1", "Hello.scala"), // 1 dep
		filepath.Join(projSrc, "dir2", "Json.scala"),  // 2 dep(1 duplicate)
	}
	expectedDeps := 1
	deps, err := sbt.parseInlineDeps(files)
	if err != nil {
		t.Errorf("expected no error but got: %s", err)
		return
	}
	assert.Len(t, deps, expectedDeps, "total expected deps mismatch")
	for _, d := range deps {
		assert.Truef(t, d.Valid(), "dependency invalid: %f", d.UID())
	}
}

func TestSbtBuilder_ParseRawDeps(t *testing.T) {
	sbt := mvnBuilder{}
	tests := []struct {
		usecase string
		dep     RawDependency
		valid   bool
	}{
		{
			usecase: "random info",
			dep: RawDependency{
				"random": "test",
			},
			valid: false,
		},
		{
			usecase: "missing group id",
			dep: RawDependency{
				"groupId": "org.gitlab",
			},
			valid: false,
		},
		{
			usecase: "missing artifact id",
			dep: RawDependency{
				"groupId": "org.gitlab",
			},
			valid: false,
		},
		{
			usecase: "valid case with version",
			dep: RawDependency{
				"groupId":    "org.gitlab",
				"artifactId": "test",
				"version":    "1.2.3",
			},
			valid: true,
		},
		{
			usecase: "valid case with meta attributes",
			dep: RawDependency{
				"groupId":    "org.gitlab",
				"artifactId": "test",
				"version":    "1.2.3",
				"exclusions": []map[string]interface{}{{
					"exclusion": map[string]interface{}{
						"groupId":    "org.gitlab",
						"artifactId": "test",
					},
				}},
			},
			valid: true,
		},
	}

	for _, variant := range tests {
		variant := variant
		t.Run(variant.usecase, func(t *testing.T) {
			deps, err := sbt.parseRawDeps([]RawDependency{variant.dep})
			if variant.valid {
				if err != nil {
					t.Errorf("expected no error but got - %s", err)
					return
				}
				assert.Len(t, deps, 1)
			} else {
				if err == nil {
					t.Errorf("expected invalid dependency error but got empty")
					return
				}
				assert.Len(t, deps, 0)
			}
		})
	}
}
