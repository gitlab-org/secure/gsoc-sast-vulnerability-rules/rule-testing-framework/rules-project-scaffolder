package build

import (
	"bytes"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func TestMSBuildBuilder_Meta(t *testing.T) {
	builder := msbuildBuilder{}
	assert.Equal(t, MSBuild, builder.Name())
	assert.Equal(t, "test.csproj", builder.Template())
	supported := []string{".cs"}
	unsupported := []string{randomExt(), randomExt(), randomExt()}
	for _, lang := range supported {
		lang := lang
		t.Run("support for "+lang, func(t *testing.T) {
			if !builder.Supports(lang) {
				t.Errorf("unsupported language")
				return
			}
		})
	}
	for _, lang := range unsupported {
		lang := lang
		t.Run("no support for "+lang, func(t *testing.T) {
			if builder.Supports(lang) {
				t.Errorf("should not have supported but is supporting")
				return
			}
		})
	}
}

func TestMSBuildBuilder_GenerateManifest(t *testing.T) {
	builder := msbuildBuilder{}
	currentDir, _ := os.Getwd()

	t.Run("no dependencies", func(t *testing.T) {
		got, err := builder.GenerateManifest(nil)
		if err != nil {
			t.Error(err)
			return
		}
		tmplPath := filepath.Join(currentDir, "..", "template", "test.csproj")
		want, err := ioutil.ReadFile(tmplPath)
		if err != nil {
			t.Errorf("read template file: %s", err)
			return
		}
		assert.True(t, bytes.Equal(got, want))
	})
}

func TestMSBuildBuilder_GenerateProject(t *testing.T) {
	currentDir, _ := os.Getwd()
	builder := msbuildBuilder{}

	t.Run("invalid options", func(t *testing.T) {
		variants := []struct {
			caseName string
			opts     ProjStructOps
		}{
			{"empty", ProjStructOps{}},
			{"no source path", ProjStructOps{
				SrcPath:   "",
				Deps:      nil,
				LangFiles: []string{"test"},
			}},
			{"no language files", ProjStructOps{
				SrcPath:   currentDir,
				LangFiles: nil,
			}},
		}
		for i := range variants {
			variant := variants[i]
			t.Run(variant.caseName, func(t *testing.T) {
				_, err := builder.GenerateProject(variant.opts)
				if err == nil {
					t.Errorf("expected error but got empty")
				}
			})
		}
	})

	t.Run("valid case: no dependencies", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "msbuild")
		langFiles, _ := getFiles(t, srcPath, builder)
		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			LangFiles: langFiles,
		}
		outPath, err := builder.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProjectMSBuild(t, outPath, opts.LangFiles, opts.AuxFiles, builder)
	})
}

func verifyGeneratedProjectMSBuild(t *testing.T, outPath string, langFiles, auxFiles []string, builder msbuildBuilder) {
	assert.DirExists(t, outPath, "generated project directory does not exist")
	outLangFiles, outAuxFiles := getFiles(t, outPath, builder)
	for _, outFile := range outLangFiles {
		filePath := strings.TrimPrefix(outFile, outPath)
		if !hasFilePath(langFiles, filePath) {
			t.Errorf("file path structure for file[%s] not retained", filePath)
			return
		}
	}

	var containsBuildFile bool // to confirm if build file is present

	for _, outFile := range outAuxFiles {
		if strings.HasSuffix(outFile, builder.Template()) {
			containsBuildFile = true
			continue
		}
		filePath := strings.TrimPrefix(outFile, outPath) // aux files are placed in the root level
		if !hasFilePath(auxFiles, filePath) {
			t.Errorf("file path structure for file[%s] not retained", filePath)
			return
		}
	}
	assert.True(t, containsBuildFile, "build file is missing")
}
