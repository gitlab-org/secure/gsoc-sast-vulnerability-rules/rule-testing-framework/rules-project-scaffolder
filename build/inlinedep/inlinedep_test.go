package inlinedep

import (
	"os"
	"path/filepath"

	"testing"

	"github.com/stretchr/testify/assert"
)

// all the dependencies used in the test fixtures
var fixtureDeps = []string{
	"com.gitlab.test-1@2.0.0",          // without version
	"com.gitlab.test-2@0.0.1-SNAPSHOT", // patch version variant
	"com.gitlab.test-3@latest",         // latest
	"com.gitlab.test-4@1.2.3",          // regular
	"com.gitlab.test-5@1.2.3",          // ...
	"com.gitlab.test-6@1.2.3",          // ...
}

type testDep struct {
	lib, ver string
	valid    bool
}

func (t testDep) value() string {
	val := t.lib
	if t.ver != "" {
		val += "@" + t.ver
	}
	return val
}

func TestParseDep(t *testing.T) {

	variants := []testDep{
		{"", "1", false},
		{"", "1.2.3", false},
		{"com.test", "abc", false},
		{"com.gitlab", "1.abc", false},
		{"blah", "1", true},
		{"com.gitlab", "1.2", true},
		{"com.gitlab", "1.2.3", true},
		{"com.gitlab", "", true}, // sets 'latest' as the version
		{"com.gitlab", "1.2.3-SNAPSHOT", true},
	}
	for i := range variants {
		variant := variants[i]
		t.Run(variant.value(), func(t *testing.T) {
			d, e := parseInlineDep(variant.value())
			if !variant.valid && e == nil || variant.valid && e != nil {
				t.Errorf("expectation mismatch")
				return
			}
			assert.Equal(t, variant.lib, d.Name())
			if variant.ver == "" {
				assert.Equal(t, fallbackDepVer, d.Ver())
			} else {
				assert.Equal(t, variant.ver, d.Ver())
			}
		})

	}
}

func TestScan(t *testing.T) {
	currentDir, _ := os.Getwd()
	fixturePath := filepath.Join(currentDir, "..", "..", "testdata", "inlinedeptest")

	fileNames := []string{
		filepath.Join(fixturePath, "File.java"),
		filepath.Join(fixturePath, "directory", "Sample.java"),
		filepath.Join(fixturePath, "directory", "inner_directory", "InnerSample.java"),
	}

	// prepare the expected data
	expected := make(map[string]InlineDependency)
	for _, str := range fixtureDeps {
		dep, e := parseInlineDep(str)
		if e != nil {
			t.Errorf("dep[%s] failed to parse dep string - error: %s", str, e)
			return
		}
		expected[dep.UID()] = dep
	}

	// perform the scan
	got, err := Scan(fileNames)
	if err != nil {
		t.Errorf("failed to scan - error: %s", err)
		return
	}

	for _, dep := range got {
		exp, ok := expected[dep.UID()]
		if !ok {
			t.Errorf("result mismatch: dep[%s] not present in expected deps", dep.UID())
			return
		}
		assert.Equal(t, exp.Name(), dep.Name())
		assert.Equal(t, exp.Ver(), dep.Ver())
	}

}
