package inlinedep

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
)

var (
	// ErrDependency occurs when the parsed dependency is not in correct format
	ErrDependency = errors.New("invalid dependency")

	// choose fallback dependency version as the latest
	fallbackDepVer = "latest"
	// the lines containing scaffold attributes must be prefixed
	// with the signatureDef in the files
	signatureDef = "scaffold:"

	// dependencies attribute in the files must be represented with depAttr
	depAttr = "dependencies"
)

// InlineDependency represents a library dependency defined in the
// comments inside language files represented in a simple
// format i.e., dependency@version. There are no meta attributes
// associated with the library dependency in the simple format.
type InlineDependency struct {
	name    string // name of the dependency
	version string // defaults to `latest` if empty
}

func (d InlineDependency) Name() string {
	return d.name
}

func (d InlineDependency) Ver() string {
	return d.version
}

func (d InlineDependency) UID() string {
	return fmt.Sprintf("%s@%s", d.name, d.version)
}

func (d *InlineDependency) validate(normalizeValues bool) error {
	if normalizeValues {
		// normalize with defaults
		d.name = strings.TrimSpace(strings.ToLower(d.name))
		if d.version == "" {
			d.version = fallbackDepVer
		}
	}
	// validate
	if d.name == "" {
		return fmt.Errorf("%w: empty name", ErrDependency)
	}
	if d.version == fallbackDepVer {
		return nil
	}
	arr := strings.Split(d.version, ".")
	if len(arr) > 0 {
		if _, e := strconv.ParseInt(arr[0], 10, 0); e != nil {
			return fmt.Errorf("%w: semver: major version", ErrDependency)
		}
	}
	if len(arr) > 1 {
		if _, e := strconv.ParseInt(arr[1], 10, 0); e != nil {
			return fmt.Errorf("%w: semver: minor version", ErrDependency)
		}
	}
	return nil
}

// IsValid indicates if the dependency has valid format
func (d InlineDependency) Valid() bool {
	return d.validate(false) == nil
}

// Scan scans inline dependencies defined in the files
func Scan(fileNames []string) ([]InlineDependency, error) {
	var deps []InlineDependency
	var wg errgroup.Group
	results := make([][]InlineDependency, len(fileNames))

	for i, name := range fileNames {
		i, name := i, name
		wg.Go(func() error {
			f, err := os.OpenFile(name, os.O_RDONLY, os.ModePerm)
			if err != nil {
				return err
			}
			defer f.Close()

			var deps []InlineDependency
			sc := bufio.NewScanner(f)
			for sc.Scan() {
				line := sc.Text()
				scannedDeps, err := scanDeps(line)
				if err != nil {
					return err
				}
				deps = append(deps, scannedDeps...)
			}
			if err := sc.Err(); err != nil {
				return errors.Wrap(err, "scan line")
			}
			results[i] = deps
			return nil
		})
	}

	if err := wg.Wait(); err != nil {
		return nil, err
	}

	for _, depArr := range results {
		deps = append(deps, depArr...)
	}

	if e := validateDeps(deps); e != nil {
		return nil, e
	}

	deps = dedupeDeps(deps) // remove duplicates

	return deps, nil
}

// parseInlineDep parses dependency string of format: dependency@version
func parseInlineDep(depStr string) (InlineDependency, error) {
	logrus.Debugf("parsing dependency string: %s", depStr)
	tmp := InlineDependency{}
	if strings.TrimSpace(depStr) == "" {
		return tmp, fmt.Errorf("%w: empty input string", ErrDependency)
	}
	// format: "dependency@version"
	vars := strings.Split(strings.TrimSpace(depStr), "@")

	// first validate the input by parsing to temporary dependency
	tmp.name = strings.TrimSpace(vars[0])
	tmp.version = fallbackDepVer
	if len(vars) > 1 {
		tmp.version = strings.TrimSpace(vars[1])
	}
	if err := tmp.validate(true /*normalizeValues*/); err != nil {
		logrus.Errorf("dependency str[%s] validation failed: %s", depStr, err)
		return tmp, err
	}
	return tmp, nil
}

// scans for dependencies defined in the given line.
// the format it looks for is:
// scaffold: dependencies=dependency@version, dependency@version,...
func scanDeps(line string) ([]InlineDependency, error) {
	var deps []InlineDependency
	// check for signature definition
	if !strings.Contains(line, signatureDef) {
		return nil, nil
	}

	// check for dependencies attributes
	split := strings.Split(line, depAttr)
	if len(split) < 2 {
		return nil, nil // deps not defined
	}

	// format: "= dependency@version, dependency@version, .."
	depsStr := strings.TrimSpace(split[1])
	depsStr = strings.TrimPrefix(depsStr, "=")

	depStrArr := strings.Split(depsStr, ",")
	for _, depStr := range depStrArr {
		depStr = strings.TrimSpace(depStr)
		if depStr == "" {
			continue
		}
		dep, err := parseInlineDep(depStr)
		if err != nil {
			return nil, errors.Wrap(err,
				fmt.Sprintf("dep string parse (%s)", depStr))
		}
		deps = append(deps, dep)
	}
	return deps, nil
}

// removes duplicate dependencies
func dedupeDeps(dupes []InlineDependency) []InlineDependency {
	ids := make(map[string]struct{}, len(dupes))
	var deduped []InlineDependency
	for _, d := range dupes {
		if _, ok := ids[d.UID()]; ok {
			continue
		}
		ids[d.UID()] = struct{}{}
		deduped = append(deduped, d)
	}
	return deduped
}

// validate each dependency for its correctness
func validateDeps(dupes []InlineDependency) error {
	for _, dep := range dupes {
		if e := (&dep).validate(true /*normalizeValues*/); e != nil {
			return fmt.Errorf("validation (%s): %w", dep.UID(), e)
		}
	}
	return nil
}
