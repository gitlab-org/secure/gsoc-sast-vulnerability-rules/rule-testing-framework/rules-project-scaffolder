package build

import (
	"fmt"
	"os"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestGetBuilder(t *testing.T) {
	variants := []struct {
		input Tool
		valid bool
	}{
		{"", false},
		{"random", false},
		{Maven, true},
	}

	for _, el := range variants {
		t.Run(fmt.Sprintf("tool: %s", el.input), func(t *testing.T) {
			val, err := GetBuilder(el.input)
			if !el.valid && err == nil || el.valid && err != nil {
				t.Errorf("expectation mismatch")
				return
			}
			if el.valid {
				assert.Equal(t, el.input, val.Name())
			} else {
				assert.ErrorIs(t, ErrToolSupport, err)
			}
		})

	}
}

func TestGenerateProject(t *testing.T) {
	tempDir := os.TempDir()
	file, _ := os.CreateTemp(tempDir, "testfile")
	defer file.Close()
	t.Run("invalid tool", func(t *testing.T) {
		_, err := GenerateProject("", ProjStructOps{SrcPath: tempDir,
			LangFiles: []string{file.Name()}})
		if !errors.Is(err, ErrToolSupport) {
			t.Errorf("expected %s, got: %s", ErrToolSupport, err)
		}
	})

	t.Run("invalid options", func(t *testing.T) {
		variants := []struct {
			caseName string
			opts     ProjStructOps
		}{
			{"empty", ProjStructOps{}},
			{"no source path", ProjStructOps{
				SrcPath:   "",
				Deps:      nil,
				LangFiles: []string{"test"},
			}},
			{"no language files", ProjStructOps{
				SrcPath:   tempDir,
				LangFiles: nil,
			}},
		}

		for i := range variants {
			variant := variants[i]
			t.Run(variant.caseName, func(t *testing.T) {
				_, err := GenerateProject(Maven, variant.opts)
				if err == nil {
					t.Errorf("expected error but got empty")
				}
			})
		}
	})
}
