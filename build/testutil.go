package build

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"math/rand"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
)

func getFiles(t *testing.T, fixtureSrcPath string, builder BuilderInfoProvider) (langFiles, auxFiles []string) {
	e := filepath.Walk(fixtureSrcPath, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if builder.Supports(filepath.Ext(path)) {
			langFiles = append(langFiles, path)
		} else {
			auxFiles = append(auxFiles, path)
		}
		return nil
	})
	if e != nil {
		t.Errorf("failed to determine files in the test fixture: %s", e)
		return
	}
	return
}

func hasFilePath(filePaths []string, filePath string) bool {
	for _, fp := range filePaths {
		if strings.HasSuffix(fp, filePath) {
			return true
		}
	}
	return false
}

func verifyGeneratedProject(t *testing.T, outPath string, langFiles, auxFiles []string, builder BuilderInfoProvider, outSrcPathSuffix ...string) {
	assert.DirExists(t, outPath, "generated project directory does not exist")
	outLangFiles, outAuxFiles := getFiles(t, outPath, builder)
	// confirm if file paths are preserved in the generated project
	outSrcPath := filepath.Join(append([]string{outPath}, outSrcPathSuffix...)...) // project files are placed in <project>/src/main/<...files..>
	for _, outFile := range outLangFiles {
		filePath := strings.TrimPrefix(outFile, outSrcPath)
		if !hasFilePath(langFiles, filePath) {
			t.Errorf("file path structure for file[%s] not retained", filePath)
			return
		}
	}

	var containsBuildFile bool // to confirm if build file is present

	for _, outFile := range outAuxFiles {
		if strings.HasSuffix(outFile, builder.Template()) {
			containsBuildFile = true
			continue
		}
		filePath := strings.TrimPrefix(outFile, outPath) // aux files are placed in the root level
		if !hasFilePath(auxFiles, filePath) {
			t.Errorf("file path structure for file[%s] not retained", filePath)
			return
		}
	}
	assert.True(t, containsBuildFile, "build file is missing")
}

// helpers

// mockDependency represents a library dependency
type mockDependency struct {
	name    string // name of the dependency
	version string // defaults to `latest` if empty
}

func (d mockDependency) Name() string {
	return d.name
}

func (d mockDependency) Ver() string {
	return d.version
}

func (d mockDependency) UID() string {
	return fmt.Sprintf("%s@%s", d.name, d.version)
}

// IsValid indicates if the dependency has valid format
func (d mockDependency) Valid() bool {
	return true
}

// reads raw dependencies defined in sacffold yaml config file
func readDepsFromCfg(cfgPath string) ([]RawDependency, error) {
	type scaffoldRepr struct {
		Dependencies []struct {
			Dependency RawDependency `yaml:"dependency"`
		} `yaml:"dependencies"`
	}
	repr := scaffoldRepr{}
	data, err := ioutil.ReadFile(cfgPath)
	if err != nil {
		return nil, fmt.Errorf("unexpected error while reading scaffold cfg - %s", err)
	}
	if err := yaml.Unmarshal(data, &repr); err != nil {
		return nil, fmt.Errorf("unexpected error while unmarshalling scaffold cfg - %s", err)
	}
	var rawDeps []RawDependency
	for _, d := range repr.Dependencies {
		rawDeps = append(rawDeps, d.Dependency)
	}
	return rawDeps, nil
}

func exists(deps []Dependency, d Dependency) bool {
	for _, el := range deps {
		if el.UID() == d.UID() {
			return true
		}
	}
	return false
}

func randomExt() string {
	chars := []byte("abcdefghijklmnopqrstuvw")

	random := ""
	for i := 0; i < 4; i++ {
		random += string(chars[rand.Intn(len(chars))])
	}
	return "." + random
}
