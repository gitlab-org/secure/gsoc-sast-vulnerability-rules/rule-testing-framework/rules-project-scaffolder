ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.gitlab"
ThisBuild / organizationName := "secure"

lazy val root = (project in file("."))
  .settings(
    name := "test-scala",
    {{ .depString }}
  )
