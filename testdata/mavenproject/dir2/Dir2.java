// License: MIT GitLab Inc.
// scaffold: dependencies=ch.qos.logback.logback-core@1.2.10
// Sample comment
import dir2;
import ch.qos.logback.core.util.Loader;

public class Dir2 extends Loader {
    public void load(String[] args) {
        System.out.println("Hello world");
    }
}