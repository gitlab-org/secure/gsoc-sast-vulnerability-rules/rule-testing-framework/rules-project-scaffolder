// License: MIT GitLab Inc.
// Sample comment
// scaffold: dependencies=org.sonatype.sisu.sisu-guice@2.1.7, ch.qos.logback.logback-core@1.2.10
import dir1;
import com.google.inject.internal.util.Preconditions;

public class Bar {
    public void test(String[] args) {
        Preconditions.checkArgument(false);
        System.out.println("Hello world");
    }
}

