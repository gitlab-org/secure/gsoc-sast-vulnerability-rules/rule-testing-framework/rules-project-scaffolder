package scaffold

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadCfg(t *testing.T) {
	t.Run("invalid file format", func(t *testing.T) {
		currentDir, _ := os.Getwd()
		src := filepath.Join(currentDir, "..", "testdata", "scaffoldcfg")
		files := []string{
			filepath.Join(src, "dir", "scaffold.txt"), // invalid ext
		}
		_, err := readCfg(files)
		if err == nil {
			t.Errorf("expected unsupported file name/extension error but got empty")
		}
	})
	t.Run("invalid file name", func(t *testing.T) {
		currentDir, _ := os.Getwd()
		src := filepath.Join(currentDir, "..", "testdata", "scaffoldcfg")
		files := []string{
			filepath.Join(src, "dir", "invalidname.yml"), // invalid name
		}
		_, err := readCfg(files)
		if err == nil {
			t.Errorf("expected unsupported file name/extension error but got empty")
		}
	})
	t.Run("valid", func(t *testing.T) {
		currentDir, _ := os.Getwd()
		src := filepath.Join(currentDir, "..", "testdata", "scaffoldcfg")
		files := []string{
			filepath.Join(src, "scaffold.yml"),         // 1 dependency
			filepath.Join(src, "dir", "scaffold.yaml"), // 2 dependencies
		}
		cfg, err := readCfg(files)
		if err != nil {
			t.Errorf("read cfg: %s", err)
			return
		}
		got := len(cfg.Dependencies)
		want := 3
		assert.Equal(t, want, got)
	})

}
