package scaffold

import (
	"errors"
	"fmt"
	"io/ioutil"
	"rules-project-scaffolder/build"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
)

// Config represents scaffold configuration file
type Config struct {
	// Dependencies all the dependencies. each array entry contains dependency
	// information in a schema supported by the build tool defined in $BUILD_TOOL
	Dependencies []build.RawDependency
}

type internalYAMLRepr struct {
	// Dependencies all the dependencies. each array entry contains dependency
	// information in a schema supported by the build tool defined in $BUILD_TOOL
	Dependencies []struct {
		Dependency build.RawDependency `yaml:"dependency"`
	} `yaml:"dependencies"`
}

// readCfg builds a scaffold Config by gathering dependencies from
// all the scaffold files
func readCfg(scaffoldFiles []string) (Config, error) {
	ctxlog := logrus.WithField("action", "read scaffold config")
	cfg := Config{}
	if len(scaffoldFiles) == 0 {
		ctxlog.Debugf("no scaffold config files found")
		return cfg, nil
	}
	ctxlog.Infof("reading %d scaffold config files..", len(scaffoldFiles))
	var deps []build.RawDependency
	for _, file := range scaffoldFiles {
		ctxlog.Infof("processing scaffold config file - path: %s ..", file)
		if !isValidCfg(file) {
			ctxlog.Errorf("config file is invalid, unsupported filename or extension")
			return cfg, errors.New("unsupported cfg file name/extension")
		}

		bytedata, err := ioutil.ReadFile(file)
		if err != nil {
			ctxlog.Errorf("failed to read config file - %s", err)
			return cfg, fmt.Errorf("could not read file: %s due to %v", file, err)
		}
		ctxlog.Debugf("file read success - parsing YAML content to native structure ..")
		tmpCfg := internalYAMLRepr{}
		if err := yaml.Unmarshal(bytedata, &tmpCfg); err != nil {
			ctxlog.Errorf("failed to parse YAML content in the config file - %s", err)
			return cfg, fmt.Errorf("could not parse YAML file: %s due to %v", file, err)
		}

		var rawDeps []build.RawDependency
		for _, d := range tmpCfg.Dependencies {
			if d.Dependency == nil {
				continue
			}
			rawDeps = append(rawDeps, d.Dependency)
		}

		ctxlog.Infof("parsed %d dependencies", len(rawDeps))
		ctxlog.Debug(rawDeps)
		deps = append(deps, rawDeps...)
	}
	cfg.Dependencies = deps
	ctxlog.Infof("parsed %d dependencies in total", len(deps))
	return cfg, nil
}
